<div class="event">
	<div class="date">
		<?php if(get_sub_field('day_of_the_week')): ?>
			<span class="day-of-the-week"><?php the_sub_field('day_of_the_week'); ?></span>
		<?php else: ?>
			<?php $date = get_sub_field('date', false, false); $date = new DateTime($date); ?>
			<span class="month"><?php echo $date->format('M'); ?></span>
			<span class="day"><?php echo $date->format('j'); ?></span>
			<span class="day-of-the-week"><?php echo $date->format('D'); ?></span>
		<?php endif; ?>
	</div>

	<?php if(get_sub_field('photo')): ?>
		<div class="photo">
			<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	<?php endif; ?>

	<div class="info">
		<div class="headline">
			<h3><?php the_sub_field('title'); ?></h3>

			<?php if(get_sub_field('time')): ?>
				<h4 class="time"><?php the_sub_field('time'); ?></h4>
			<?php endif; ?>

			<?php if(get_sub_field('cost')): ?>
				<h4 class="cost"><?php the_sub_field('cost'); ?></h4>
			<?php endif; ?>									
		</div>

		<div class="description">
			<?php the_sub_field('description'); ?>
		</div>

		<?php if(get_sub_field('cta_link')): ?>
			<div class="cta">
				<a href="<?php the_sub_field('cta_link'); ?>" rel="external"><?php the_sub_field('cta_label'); ?></a>
			</div>
		<?php endif; ?>	
	</div>
	
</div>