<?php include( locate_template( 'partials/header/global-variables.php', false, false ) ); ?>

<div class="next-door-info">

	<div class="copy">
		<?php the_field('next_door_copy'); ?>
	</div>

	<div class="graphics">		
		<div class="logo">
			<?php if(get_field('next_door_link')): ?><a href="<?php the_field('next_door_link'); ?>" rel="external"><?php endif; ?>
				<img src="<?php $image = get_field('next_door_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php if(get_field('next_door_link')): ?></a><?php endif; ?>
		</div>
	</div>

</div>