<?php

/*

	Template Name: Calendar

*/

get_header(); ?>

	<section id="main">
		<div class="wrapper">
			
			<section class="section-header">
				<h1><?php the_field('calendar_info_headline'); ?></h1>

				<div class="copy">
					<?php the_field('calendar_info'); ?>
				</div>				
			</section>


			<section class="special-events events">
				<div class="events-header">
					<h2>Special Events</h2>
				</div>		
	
				<?php if(have_rows('special_events')): while(have_rows('special_events')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'event' ): ?>

				    	<?php get_template_part('partials/event'); ?>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
			</section>


			<section class="weekly-events events">
				<div class="events-header">
					<h2>Weekly Events</h2>
				</div>		

				<?php if(have_rows('weekly_events')): while(have_rows('weekly_events')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'event' ): ?>

				    	<?php get_template_part('partials/event'); ?>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>